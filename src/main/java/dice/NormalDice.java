package dice;

public class NormalDice extends Dice {
    private static NormalDice instance;

    private NormalDice() {
    }

    public static NormalDice getInstance() {
        if(instance == null) {
            instance = new NormalDice();
        }
        return instance;
    }

    public int roll() {
        return random.nextInt(6) + 1;
    }
}
