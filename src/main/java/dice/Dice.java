package dice;

import java.util.Random;

public abstract class Dice {
    protected Random random = new Random();
    public abstract int roll();
}
