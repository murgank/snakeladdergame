package dice;

public class CrookedDice extends Dice{
    private static CrookedDice instance;

    private CrookedDice() {
    }

    public static CrookedDice getInstance() {
        if(instance == null) {
            instance = new CrookedDice();
        }
        return instance;
    }

    public int roll() {
        return (random.nextInt(6) + 1) * 2;
    }
}
