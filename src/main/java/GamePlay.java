import board.GameBoard;
import dice.Dice;

public class GamePlay {
    private final GameBoard board;
    private final Dice dice;
    private final Player player;

    public GamePlay(GameBoard board, Dice dice, Player player) {
        this.board = board;
        this.dice = dice;
        this.player = player;
    }

    public void move() {
        int moveBy = dice.roll();
        int latestPosition = board.getPositionAfterMove(player.getPosition(),moveBy);
        player.setPosition(latestPosition);
    }
}
