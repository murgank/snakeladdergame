package board;

import java.util.HashMap;

public class GameBoardBuilder {
    private final int size;
    private HashMap<Integer,Integer> snakeAndLadder = new HashMap<>();

    public GameBoardBuilder(int size) {
        this.size = size;
    }

    public GameBoardBuilder addSnakeOrLadder(int startPosition, int endPosition) {
        snakeAndLadder.put(startPosition,endPosition);
        return this;
    }

    public GameBoard getGameBoard() {
        return new GameBoard(size, snakeAndLadder);
    }
}
