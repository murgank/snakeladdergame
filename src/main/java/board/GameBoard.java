package board;

import dice.Dice;

import java.util.HashMap;

public class GameBoard {
    private final int size;
    private final HashMap<Integer,Integer> snakeAndLadder;

    public GameBoard(int size, HashMap<Integer, Integer> snakeAndLadder) {
        this.size = size;
        this.snakeAndLadder = snakeAndLadder;
    }

    public int getPositionAfterMove(int currentPosition, int moveBy) {
        int latestPosition = currentPosition + moveBy;
        if(latestPosition > size) {
            return currentPosition;
        }
        if(snakeAndLadder.containsKey(latestPosition)) {
            return snakeAndLadder.get(latestPosition);
        }
        return latestPosition;
    }
}
