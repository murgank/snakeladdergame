import board.GameBoard;
import board.GameBoardBuilder;
import dice.CrookedDice;
import dice.Dice;
import dice.NormalDice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GamePlayTest {
    @Test
    void shouldMovePlayer() {
        Player player = new Player();
        GameBoard gameBoard = new GameBoardBuilder(100).getGameBoard();
        Dice dice = NormalDice.getInstance();
        GamePlay game = new GamePlay(gameBoard,dice,player);

        game.move();

        assertTrue(player.getPosition() > 1);
    }

    @Test
    void shouldNotMoveIfPlayerPositionBecomesGreaterThanBoardSize() {
        Player player = new Player();
        player.setPosition(100);
        GameBoard gameBoard = new GameBoardBuilder(100).getGameBoard();
        Dice dice = NormalDice.getInstance();
        GamePlay game = new GamePlay(gameBoard,dice,player);

        game.move();

        Assertions.assertEquals(player.getPosition(), 100);
    }

    @Test
    void shouldMoveToSnakeTailIfCurrentPositionHasSnake() {
        Player player = new Player();
        GameBoard gameBoard = new GameBoardBuilder(100)
                .addSnakeOrLadder(14,7)
                .getGameBoard();

        Dice dice = new Dice() {
            @Override
            public int roll() {
                return 13;
            }
        };
        GamePlay game = new GamePlay(gameBoard,dice,player);

        game.move();

        Assertions.assertEquals(player.getPosition(), 7);
    }

    @Test
    void shouldBeAbleToStartGameWithCrookedDice() {
        Player player = new Player();
        GameBoard gameBoard = new GameBoardBuilder(100)
                .getGameBoard();
        Dice dice = CrookedDice.getInstance();
        GamePlay game = new GamePlay(gameBoard,dice,player);

        game.move();

        assertEquals(player.getPosition()%2,1);
    }

    @Test
    void runGameFor10Turns() {
        Player player = new Player();
        GameBoard gameBoard = new GameBoardBuilder(100)
                .addSnakeOrLadder(14,7)
                .getGameBoard();
        Dice dice = CrookedDice.getInstance();
        GamePlay game = new GamePlay(gameBoard,dice,player);

        System.out.println("--------------GAME STARTED-----------");

        for(int i=0;i<10;i++) {
            game.move();
            System.out.println("Current Position " + player.getPosition());
        }
        System.out.println("--------------GAME ENDED-----------");
    }
}