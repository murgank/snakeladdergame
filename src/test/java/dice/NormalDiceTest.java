package dice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NormalDiceTest {
    @Test
    void shouldGetSingleInstanceOfDice() {
        NormalDice diceOne = NormalDice.getInstance();
        NormalDice diceTwo = NormalDice.getInstance();

        assertEquals(diceOne,diceTwo);
    }

    @Test
    void shouldGenerateRandomNumberBetween1to6() {
        int randomNumber = NormalDice.getInstance().roll();

        assertTrue(randomNumber >= 1 && randomNumber <=6);
    }
}