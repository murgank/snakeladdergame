package dice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CrookedDiceTest {

    @Test
    void shouldGetSingleInstanceOfDice() {
        CrookedDice diceOne = CrookedDice.getInstance();
        CrookedDice diceTwo = CrookedDice.getInstance();

        assertEquals(diceOne,diceTwo);
    }

    @Test
    void shouldGenerateRandomEvenNumberBetween2to12() {
        int randomNumber = CrookedDice.getInstance().roll();

        assertTrue(randomNumber >= 2 && randomNumber <=12);
        assertEquals(randomNumber % 2, 0);
    }
}