package board;

import dice.Dice;
import dice.NormalDice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class GameBoardTest {
    @Test
    void shouldGetPositionAfterMove() {
        GameBoard gameBoard = new GameBoardBuilder(100).getGameBoard();

        int result = gameBoard.getPositionAfterMove(1,1);

        assertEquals(result,2);
    }

    @Test
    void shouldNotMoveIfLatestPositionBecomesGreaterThanBoardSize() {
        GameBoard gameBoard = new GameBoardBuilder(100).getGameBoard();

        int result = gameBoard.getPositionAfterMove(96,6);

        assertEquals(result,96);
    }

    @Test
    void shouldMoveToSnakeTailIfCurrentPositionHasSnake() {
        GameBoard gameBoard = new GameBoardBuilder(100)
                .addSnakeOrLadder(14,7)
                .getGameBoard();

        int result = gameBoard.getPositionAfterMove(13,1);

        assertEquals(result,7);
    }
}