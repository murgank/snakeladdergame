package board;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameBoardBuilderTest {
    @Test
    void shouldBuildSnakeAndLadderGameBoard() {
        GameBoard board = new GameBoardBuilder(100)
                .getGameBoard();

        assertNotNull(board);
    }

    @Test
    void shouldBuildSnakeAndLadderGameBoardWithASnake() {
        GameBoard board = new GameBoardBuilder(100)
                .addSnakeOrLadder(14,7)
                .getGameBoard();

        assertNotNull(board);
    }
}